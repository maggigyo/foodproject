//
//  MERMainViewController.m
//  foodProject
//
//  Created by ITHS on 2016-03-10.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERMainViewController.h"

@interface MERMainViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pic1;
@property (weak, nonatomic) IBOutlet UILabel *pic2;

@end

@implementation MERMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.5f
                          delay:0.9f
                        options:(UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat |
                                 UIViewAnimationOptionAllowUserInteraction)//prevents UI from freezing
                     animations:^{
                         int old1y = self.pic1.center.y;
                         CGPoint new = CGPointMake(self.pic1.center.x, old1y + 6);
                         self.pic1.center = new;
                         int old2y = self.pic2.center.y;
                         new = CGPointMake(self.pic2.center.x, old2y + 6);
                         self.pic2.center = new;
                     }
                     completion:nil
     ];
    self.navigationController.navigationBar.hidden = YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

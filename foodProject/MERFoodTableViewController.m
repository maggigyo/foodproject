//
//  MERFoodTableViewController.m
//  foodProject
//
//  Created by ITHS on 2016-02-25.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERFoodTableViewController.h"
#import "MERTableViewCell.h"
#import "MERDetailViewController.h"

@interface MERFoodTableViewController ()
@property(nonatomic) NSArray *foodData;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) NSArray *searchResult;

@end

@implementation MERFoodTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *URL = [NSURL URLWithString:@"http://matapi.se/foodstuff?nutrient"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:
        ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSError *parseError;
            
            //Will get an array of dictionaries
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                            error:&parseError];
            self.foodData = json;
            [self.tableView reloadData];
            
        }];
    
    [task resume];
    
    self.searchController = [[UISearchController alloc]
                             initWithSearchResultsController:nil];
    
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchResultsUpdater = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    

}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchText = searchController.searchBar.text;
    
    //SKAPA HJÄLPMETOD SENARE FÖR DETTA?:
    
    NSPredicate *findFood = [NSPredicate predicateWithFormat: @"name contains[c] %@", searchText];
    self.searchResult = [self.foodData filteredArrayUsingPredicate:findFood];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.searchController.isActive && self.searchController.searchBar.text.length >0){
        return self.searchResult.count;
    }else{
        return self.foodData.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MERTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodCell"
                                                            forIndexPath:indexPath];
    NSArray *activeArray;//To avoid verbose duplication
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length >0){
        activeArray = self.searchResult;
    }else{
        activeArray = self.foodData;
    }
    cell.textLabel.text = activeArray[indexPath.row][@"name"];
    cell.foodStats = activeArray[indexPath.row];
    return cell;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     MERDetailViewController *dest = [segue destinationViewController];
     if([segue.identifier  isEqual: @"detail"]){
         MERTableViewCell *cell = sender;
         dest.foodNumber = cell.foodStats[@"number"];
     }

 }


@end

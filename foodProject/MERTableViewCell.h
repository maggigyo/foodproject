//
//  MERTableViewCell.h
//  foodProject
//
//  Created by ITHS on 2016-03-09.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MERTableViewCell : UITableViewCell
//Låt ev. denna vara av en särskild klass, och data arrayen innehålla instanser av denna klass.
@property NSDictionary *foodStats;
@end

//
//  MERLaunchViewController.m
//  foodProject
//
//  Created by ITHS on 2016-03-29.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERLaunchViewController.h"

@interface MERLaunchViewController ()

@end

@implementation MERLaunchViewController

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
    [NSThread sleepForTimeInterval:5.0];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  MERAboutViewController.m
//  foodProject
//
//  Created by ITHS on 2016-03-11.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERAboutViewController.h"

@interface MERAboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pic1;
@property (weak, nonatomic) IBOutlet UILabel *pic2;
@property (weak, nonatomic) IBOutlet UILabel *pic3;
@property (weak, nonatomic) IBOutlet UILabel *decorText;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIDynamicItemBehavior *itemBehavior;
@end

@implementation MERAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.pic1, self.pic2, self.pic3]];
    [self.animator addBehavior:self.gravity];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.pic1, self.pic2, self.pic3,]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    self.itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:
                         @[self.pic1, self.pic2, self.pic3]];
    self.itemBehavior.elasticity = 0.7;//Make them bouncy!
    [self.animator addBehavior:self.itemBehavior];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

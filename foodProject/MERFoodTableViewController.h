//
//  MERFoodTableViewController.h
//  foodProject
//
//  Created by ITHS on 2016-02-25.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MERFoodTableViewController : UITableViewController <UISearchResultsUpdating>

@end

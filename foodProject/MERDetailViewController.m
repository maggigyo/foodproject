//
//  MERDetailViewController.m
//  foodProject
//
//  Created by ITHS on 2016-02-25.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERDetailViewController.h"

@interface MERDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *kcal;
@property (weak, nonatomic) IBOutlet UILabel *fat;
@property (weak, nonatomic) IBOutlet UILabel *protein;
@property (weak, nonatomic) IBOutlet UILabel *sugar;
@property (weak, nonatomic) IBOutlet UILabel *carbs;
@property (weak, nonatomic) IBOutlet UILabel *iron;
@property (weak, nonatomic) IBOutlet UILabel *calcium;
@property (weak, nonatomic) IBOutlet UILabel *vitC;
@property (weak, nonatomic) IBOutlet UILabel *vitK;
@property (weak, nonatomic) IBOutlet UILabel *vitD;
@property (weak, nonatomic) IBOutlet UILabel *vitB6;
@property (weak, nonatomic) IBOutlet UILabel *vitB12;
@property (weak, nonatomic) IBOutlet UILabel *vitE;
@property (weak, nonatomic) IBOutlet UILabel *healthiness;

@property(nonatomic) NSDictionary *foodInfo;

@end

@implementation MERDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *link = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@?nutritient", self.foodNumber];
    NSURL *URL = [NSURL URLWithString:link];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      NSError *parseError;
                                      
                                      //Will get an array of dictionaries
                                      self.foodInfo = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
                                      
                                      //Main queue for UI access
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          //For moving code out of viewDidLoad
                                          [self assignAllLabels];
                                      });
                                      
                                  }];
    
    [task resume];//Start task
}

//Gives all labels values from the dictionary foodInfo
-(void)assignAllLabels{
    self.foodName.text = self.foodInfo[@"name"];
    NSDictionary *nutValues = self.foodInfo[@"nutrientValues"];
    
    self.kcal.text = [NSString stringWithFormat:@"Kcal: %@",
                      [self fix: nutValues[@"energyKcal"]]];
    self.fat.text = [NSString stringWithFormat:@"Fett: %@",
                     [self fix: nutValues[@"fat"]]];
    self.protein.text = [NSString stringWithFormat:@"Protein: %@",
                         [self fix: nutValues[@"protein"]]];
    self.sugar.text = [NSString stringWithFormat:@"Socker: %@",
                       [self fix: nutValues[@"sugar"]]];
    self.carbs.text = [NSString stringWithFormat:@"Kolhydrater: %@",
                       [self fix: nutValues[@"carbohydrates"]]];
    self.iron.text = [NSString stringWithFormat:@"Järn: %@",
                      [self fix: nutValues[@"iron"]]];
    self.calcium.text = [NSString stringWithFormat:@"Calcium: %@",
                         [self fix: nutValues[@"calcium"]]];
    self.vitC.text = [NSString stringWithFormat:@"C: %@",
                      [self fix: nutValues[@"vitaminC"]]];
    self.vitK.text = [NSString stringWithFormat:@"K: %@",
                      [self fix: nutValues[@"vitaminK"]]];
    self.vitD.text = [NSString stringWithFormat:@"D: %@",
                      [self fix: nutValues[@"vitaminD"]]];
    self.vitB6.text = [NSString stringWithFormat:@"B6: %@",
                       [self fix: nutValues[@"vitaminB6"]]];
    self.vitB12.text = [NSString stringWithFormat:@"B12: %@",
                        [self fix:nutValues[@"vitaminB12"]]];
    self.vitE.text = [NSString stringWithFormat:@"E: %@",
                      [self fix: nutValues[@"vitaminE"]]];
    
    long h = [self calculateHealthiness];
    self.healthiness.text = [NSString stringWithFormat:@"Nyttighet: %ld", h];
    //I should probably make something that handles (null)
    //It looks sloppy
}

-(NSString*)fix:(id)id{
    NSString *idString = [NSString stringWithFormat:@"%@",id];
    if([idString characterAtIndex:0] == '('){ //(null)
        idString = @"?";
    }else if (idString.length > 5){ //Needs to be shortened
        idString = [idString substringToIndex:5];
    }
    return idString;
}

//Calculates the value of healthiness
-(long)calculateHealthiness{
    NSDictionary *nutValues = self.foodInfo[@"nutrientValues"];
    long healthiness = [nutValues[@"protein"]integerValue]+[nutValues[@"iron"]integerValue]*1.5
    +[nutValues[@"vitaminC"]integerValue]*1.5 + [nutValues[@"vitaminK"]integerValue]*1.5
    +[nutValues[@"vitaminD"]integerValue]*1.5 + [nutValues[@"vitaminB6"]integerValue]*1.5
    +[nutValues[@"vitaminB12"]integerValue]*1.5 + [nutValues[@"vitaminE"]integerValue]*1.5
    -[nutValues[@"fat"]integerValue]*2 - [nutValues[@"sugar"]integerValue];
    return healthiness;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}
@end
